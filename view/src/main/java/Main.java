import javax.xml.bind.JAXBException;
import java.io.File;
import java.util.Scanner;

/**
 * Created by Nastya on 12.07.2016.
 */
public class Main {
    public static void main(String[] args) throws JAXBException {
        System.out.println("Please, enter directory with requests (smth like: C:\\Users\\requests)");
        Scanner scanner = new Scanner(System.in);
        String pathR, pathA, message, pointer;
        pathR = scanner.nextLine();
        System.out.println("Please, enter directory to save your answers");
        pathA = scanner.nextLine();
        boolean file1 = new File(pathA).mkdir();
        System.out.println("If you want to end up session, press 0; if you want to choose another directory, enter 'new'; for continue press 'enter'");
        int k=1;
        while (true) {
            pointer = scanner.nextLine();
            if (pointer.equals("0")) break;
            if (pointer.equals("new")){
                System.out.println("Please, enter directory with requests");
                pathR = scanner.nextLine();
            }
            File []fList;
            File F = new File(pathR);
            fList = F.listFiles();
            for(int i=0; i<fList.length; i++){
                if(fList[i].isFile()){
                    if (fList[i].getName().contains(".xml")&& !SaveLoadMessage.loadMessageMR(fList[i].getPath()).getMessageHeader().getMessageld().equals("2374")){
                        message=SaveLoadMessage.loadMessageMR(fList[i].getPath()).getMessageHeader().getMessageld();
                        StringBuilder randString = new StringBuilder();
                        int count = (int) (Math.random() * 30);
                        for (int j = 0; j < count; j++)
                            randString.append(message.charAt((int) (Math.random() * message.length())));
                        SaveLoadMessage.saveMessageA(randString.toString(), message, pathA + "answer" + k + ".xml");
                        k++;
                    }
                    else {
                        System.out.println("file: "+fList[i].getName()+" do not implement our scheme");
                    }
                }
            }
            System.out.println("Directory is treated, choose '0' or 'new'");
        }
        System.out.println("Session is successfully ended");
    }
}
