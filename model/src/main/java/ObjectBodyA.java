import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by Nastya on 13.07.2016.
 */
@XmlRootElement
@XmlType(propOrder = {"objectBody"})
public class ObjectBodyA {
    private Receipt objectBody;

    public ObjectBodyA(){
        objectBody = new Receipt();
    }

    public Receipt getObjectBody(){
        return objectBody;
    }

    @XmlElement
    public void setObjectBody(Receipt objectBody){
        this.objectBody=objectBody;
    }
}
