import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by Nastya on 12.07.2016.
 */
@XmlRootElement
@XmlType(propOrder = {"messageld"})
public class MessageldR {
    private String messageld;

    public MessageldR(){
        messageld="2374";
    }

    public MessageldR(String messageld){
        this.messageld=messageld;
    }

    @XmlElement
    public void setMessageld(String messageld){
        this.messageld=messageld;
    }

    public String getMessageld(){
        return messageld;
    }
}
