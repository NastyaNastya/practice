import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by Nastya on 12.07.2016.
 */
@XmlRootElement
@XmlType(propOrder = {"objectBody"})
public class ObjectBodyR {
    private String objectBody;

    public ObjectBodyR() {
        objectBody = " ";
    }

    @XmlElement
    public void setObjectBody(String objectBody) {
        this.objectBody = objectBody;
    }

    public String getObjectBody() {
        return objectBody;
    }
}

