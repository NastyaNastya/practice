import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by Nastya on 13.07.2016.
 */
@XmlRootElement
@XmlType(propOrder = {"receipt"})
public class Receipt {
    private String receipt;

    public Receipt() {
        receipt = " ";
    }

    @XmlElement
    public void setReceipt(String receipt) {
        this.receipt = receipt;
    }

    public String getReceipt() {
        return receipt;
    }
}
