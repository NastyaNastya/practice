import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by Nastya on 13.07.2016.
 */
@XmlRootElement
@XmlType(propOrder = {"messageHeader","messageBody"})
public class MessageA {
    private MessageldA messageHeader;
    private ObjectBodyA messageBody;

    public MessageA(){
        messageHeader =new MessageldA();
        messageBody =new ObjectBodyA();
    }

    public MessageA(String messageld, String correlationld){
        messageHeader =new MessageldA(messageld,correlationld);
        messageBody =new ObjectBodyA();
    }

    public MessageldA getMessageHeader(){
        return messageHeader;
    }

    @XmlElement
    public void setMessageHeader(MessageldA messageHeader){
        this.messageHeader =messageHeader;
    }

    public ObjectBodyA getMessageBody(){
        return messageBody;
    }

    @XmlElement
    public void setMessageBody(ObjectBodyA messageBody){
        this.messageBody =messageBody;
    }
}
