import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by Nastya on 13.07.2016.
 */
@XmlRootElement
@XmlType(propOrder = {"messageld", "correlationld"})
public class MessageldA {
    private String messageld;
    private String correlationld;

    public MessageldA(){
        messageld ="11111";
        correlationld="22222";
    }

    public MessageldA(String messageld, String correlationld){
        this.messageld = messageld;
        this.correlationld=correlationld;
    }

    @XmlElement
    public void setMessageld(String messageld){
        this.messageld = messageld;
    }

    public String getMessageld(){
        return messageld;
    }

    @XmlElement
    public void setCorrelationld(String correlationld){
        this.correlationld=correlationld;
    }

    public String getCorrelationld(){
        return correlationld;
    }
}
