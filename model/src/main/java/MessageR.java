import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by Nastya on 12.07.2016.
 */
@XmlRootElement
@XmlType(propOrder = {"messageHeader","messageBody"})
public class MessageR {
    private MessageldR messageHeader;
    private ObjectBodyR messageBody;

    public MessageR(){
        messageHeader =new MessageldR();
        messageBody =new ObjectBodyR();
    }

    public MessageR(String messageld){
        messageHeader =new MessageldR(messageld);
        messageBody =new ObjectBodyR();
    }

    public MessageldR getMessageHeader(){
        return messageHeader;
    }

    @XmlElement
    public void setMessageHeader(MessageldR messageHeader){
        this.messageHeader = messageHeader;
    }

    public ObjectBodyR getMessageBody(){
        return messageBody;
    }

    @XmlElement
    public void setMessageBody(ObjectBodyR messageBody){
        this.messageBody =messageBody;
    }
}
