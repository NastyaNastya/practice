import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

/**
 * Created by Nastya on 12.07.2016.
 */
public class SaveLoadMessage {

    public static void saveMessageR(String messageld, String path) throws JAXBException {
        MessageR message = new MessageR(messageld);
        File newFile = new File(path);
        JAXBContext jaxbContext = JAXBContext.newInstance(MessageR.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(message, newFile);
    }

    public static String loadMessage(String path) throws JAXBException {
        String messageld="";
        MessageR messageR = new MessageR();
        File newFile = new File(path);
        JAXBContext jaxbContext = JAXBContext.newInstance(MessageR.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        messageR = (MessageR) unmarshaller.unmarshal(newFile);
        messageld = messageR.getMessageHeader().getMessageld();
        return messageld;
    }

    public static void saveMessageA(String randStr, String messageld, String path) throws JAXBException {
        MessageA messageA = new MessageA(randStr,messageld);
        File newFile = new File(path);
        JAXBContext jaxbContext = JAXBContext.newInstance(MessageA.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,true);
        jaxbMarshaller.marshal(messageA,newFile);
    };

    public static MessageR loadMessageMR(String path){
        MessageR messageR = new MessageR();
        File newFile = new File(path);
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(MessageR.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            messageR = (MessageR) unmarshaller.unmarshal(newFile);

        } catch (JAXBException e){
            System.out.println("loadMessageMR threw JAXBException"+e);
        }
        return messageR;
    }
}
